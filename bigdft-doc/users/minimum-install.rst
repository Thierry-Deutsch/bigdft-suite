.. _minimum install:

Minimum Install Walkthrough
===========================

This install was performed on Ubuntu 20.04 LTS running on WSL2 (Windows Subsystem for Linux 2).

.. note::

	This guide will be valid for regular Ubuntu systems, while also providing support for Users 
	in a Windows environment. For information on installing WSL2, see the 
	`Microsoft docs for WSL <https://docs.microsoft.com/en-us/windows/wsl/install>`_
	
Install from a Fresh system
---------------------------

These instructions were developed and verifed on a clean install, so for best results it may be 
worth considering such a system, if it is possible.

Steps
-----

The following steps will prepare your system for a BigDFT install. This is a minimal list, meaning
that several of the dependencies for these libraries are also dependencies for BigDFT.

#. ``sudo apt update && sudo apt upgrade``
  
#. ``sudo apt install build-essential -y``
  
#. ``sudo apt install libblas-dev liblapack-dev -y``

#. ``sudo apt install cmake -y``

#. ``sudo apt install openmpi-bin -y``

  
.. warning::
	
	Ensure that ``python`` launches python 3. This can be done via the command:	``python --version``
	
	If this is not the case, then python-is-python3 is the easiest way to solve the problem:
	
	.. code:: bash
	
		sudo apt install python-is-python3 -y
	
Once this is complete you are ready to install BigDFT. Start by following the basic install
steps on the previous page:

.. code:: bash

    git clone https://gitlab.com/l_sim/bigdft-suite.git
    mkdir build
    cd build
    python ../bigdft-suite/Installer.py -f ../bigdft-suite/rcfiles/ubuntu_MPI.rc autogen
    python ../bigdft-suite/Installer.py -f ../bigdft-suite/rcfiles/ubuntu_MPI.rc build
	
Once the build is complete you should be able to continue with the tutorial pages, or your
own projects!

If the build process fails, you may be able to find a solution to your problem on the 
:ref:`errors and fixes` page.