import subprocess
import os

from warnings import warn


def _userhost(user, host, sep='@'):
    """skip"""
    if user is None:
        return host
    else:
        return sep.join(user, host)


class Flags:
    """
    Basic but flexible handler for terminal command flags

    Arguments:
        initial_flags (str):
            initial base flags to be used and modified if needed
        prefix (optional, str):
            the non-argument prefix for these flags (-, --, etc.)
    """

    def __init__(self, initial_flags: str = '', prefix: str = None):

        self._prefix = ''
        if prefix is None:
            if '-' in initial_flags:
                self.prefix = '-' * initial_flags.count('-')
            else:
                self.prefix = '-'
        else:
            self.prefix = prefix

        self.flags = initial_flags

    def __repr__(self):
        return f'Flags({self.flags})'

    def __add__(self, other):
        self._flags = self._flags + other.strip('-')

    def __iadd__(self, other):
        self.__add__(other)
        return self

    def __sub__(self, other):
        """
        Subtract unique flags in other once.

        >>> f = Flags('aaa')
        >>> f -= 'a'
        >>> f.flags
        >>> '-aa'
        """
        for char in ''.join(set(other)):
            self._flags = self._flags.replace(char, '', 1)

    def __isub__(self, other):
        self.__sub__(other)
        return self

    @property
    def flags(self):
        """Returns the fully qualified flags as a string"""
        if len(self._flags) == 0:
            return ''
        return self.prefix + self._flags

    @flags.setter
    def flags(self, inp):
        """Set the flags to the new input

        Arguments:
            inp (str):
                new flags to use (will overwrite old ones)
        """

        for char in set(self.prefix):
            inp = inp.strip(char)
        self._flags = inp

    @property
    def prefix(self):
        """Returns the - prefix used for these flags"""
        return self._prefix

    @prefix.setter
    def prefix(self, prefix):
        """Sets the new - prefix level

        Arguments:
            prefix (str):
                new prefix to preceed flags with
        """
        self._prefix = prefix


class URL:
    """container to store the connection info for a Remote run

    Arguments:
        user (str):
            username for the remote system
        host (str):
            host address of the remote system
        port (int/str):
            port to connect to for ssh tunnels
    """

    _tunnel_port_range = (1000, 10000)

    def __init__(self,
                 user: str = None,
                 host: str = None,
                 port: int = None,
                 rsync: str = None,
                 verbose: bool = False):

        # connection details
        self._conn = {'user': user,
                      'host': host,
                      'port': port}

        self._validation = {}

        if rsync is None:
            rsync = 'auv'
        self._rsync_flags = Flags(rsync)

        self._verbose = verbose

    def __repr__(self):
        if self.local:
            return 'localhost'
        if self.is_tunnel:
            return f'URL(host={self.host}, port={self.port})'
        return f'URL(user={self.user}, host={self.host})'

    @classmethod
    def from_string(cls,
                    string: str,
                    rsync: str = None,
                    verbose: bool = False):
        """create a connection from a 'user@hostname' string

        Arguments:
            string (str):
                a user@hostname string to create a connection from
            rsync (str, optional):
                initial flags for rsync
            verbose (bool, optional):
                sets verbosity for connection actions

        Returns:
            URL
        """

        if string is None:
            return cls(verbose=verbose)

        userhost = string.split('@')
        user = userhost[0] if len(userhost) == 2 else None
        host = userhost[-1]  # in case the user is not specified

        return cls(user=user, host=host, verbose=verbose)

    @classmethod
    def from_tunnel(cls,
                    host: str,
                    user: str = None,
                    port: int = None,
                    endpoint: str = None,
                    rsync: str = None,
                    background: bool = False):
        """
        Create a URL instance based on an ssh tunnel, giving the needed command

        Guides the user through tunnel creation

        Arguments:
            user (str):
                username for remote host. Optional if already defined
            host (str):
                hostname to connect to. Optional if already defined
            port (int):
                local port to connect to. Will be assigned if not provided
            endpoint (str):
                specify a local tunnel endpoint. Defaults to 127.0.0.10
            rsync (str):
                rsync flags to be given to URL, if needed
            background (bool, optional):
                whether the process should be placed in the background
                WARNING: The process _really should_ be killed when not needed

        Returns:
            URL
        """

        # LG: user can be omitted (useful if the info is in ssh config)
        # if user is None:
        #     raise ValueError('Please provide a username')

        if host is None:
            raise ValueError('Please provide a hostname')

        if port is None:
            # port = random.randint(*URL._tunnel_port_range)
            port = 1800

        if endpoint is None:
            endpoint = '127.0.0.10'

        bg = 'f' if background else ''

        userhost = _userhost(user, host)

        tunnelstring = f'ssh {userhost} -L ' \
                       f'{endpoint}:{port}:{host}:22 -{bg}N'

        inputmsg = f"""Use the following command to create the tunnel:
        \n{tunnelstring}\n
        \nThen attempt the following command (in a separate terminal if needed)
        \nssh -p {port} {endpoint} "ls"\n
        \ncontinue (y/n)? """

        cont = ''
        timeout = 10
        count = 0
        while cont.lower() != 'y':
            count += 1
            if count > timeout:
                raise Exception('tunnel creation failed')
            cont = input(inputmsg)
            if cont == 'n':
                raise Exception('tunnel creation exited')

        return cls(user, endpoint, port, rsync)

    def userhost(self, sep='@'):
        return _userhost(self.user, self.host, sep)

    @property
    def user(self):
        """username attribute"""
        return self._conn['user']

    @user.setter
    def user(self, user):
        """set the username attribute"""
        self._conn['user'] = user

    @property
    def host(self):
        """hostname attribute"""
        return self._conn['host']

    @host.setter
    def host(self, host):
        """set the hostname attribute"""
        self._conn['host'] = host

    @property
    def port(self):
        """port to connect to for tunnels"""
        return self._conn['port']

    @port.setter
    def port(self, port):
        """set the port attribute"""
        self._conn['port'] = port

    @property
    def verbose(self):
        """attribute for the verbosity"""
        return self._verbose

    @verbose.setter
    def verbose(self, verbose):
        """set the verbosity"""
        self._verbose = verbose is True

    @property
    def local(self):
        """attribute dictating if this is a 'local' connection"""
        # use the lack of a hostname to determine
        # as user and port can be either
        return self._conn['host'] is None

    @property
    def is_tunnel(self):
        """attribute dictating if we intend to connect to an ssh tunnel"""
        return self._conn['port'] is not None

    @property
    def rsync_flags(self):
        return self._rsync_flags.flags

    @rsync_flags.setter
    def rsync_flags(self, flags):
        self._rsync_flags.flags = flags

    @property
    def ssh(self):
        """ssh command to used for this connection"""
        ret = ['ssh ']
        if self.is_tunnel:
            ret.append(f'-p {self.port} ')
        else:
            userhost = self.userhost()
            ret.append(f'{userhost} ')
        return ''.join(ret)

    def cmd(self, cmd, asynchronous=False, local=None, verbose=None):
        """execute a command on the remote

        Arguments:
            cmd (str):
                the command to be executed
            asynchronous (bool):
                do not wait for stdout if async
            local (bool):
                force local or remote execution
            verbose (bool):
                verbosity override for this execution

        Returns:
            (str):
                stdout if not async, None otherwise
        """
        if verbose is None:
            verbose = self.verbose

        # ensure the command is being executed where we need
        if local is not None:
            if not local and 'rsync' not in cmd:
                cmd = f'{self.ssh} "{cmd}"'

        elif not self.local and 'rsync' not in cmd:
            cmd = f'{self.ssh} "{cmd}"'

        if verbose:
            print('executing command: ' + repr(cmd))
        sub = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                               shell=True, text=True)
        if not asynchronous:
            ret, err = sub.communicate()
            if err is not None:
                warn(str(err), UserWarning)
            if verbose:
                print('return: ' + ret)
            return str(ret)
        else:
            return None

    def ls(self, target='', local=False):
        """perform ls command on target

        Arguments:
            target (str):
                target entity (folder or file)
            local (bool):
                force local or remote search

        Returns:
            list:
                newline split result
        """
        return [f for f in
                self.cmd(f'ls {target}', local=local).split('\n') if f != '']

    def rsync(self, files, origin, target=None, push=True):
        """create rsync "link" between origin and target, pull or push files

        Arguments:
            files (list):
                list of filenames to transfer
            origin (str):
                origin folder
            target (str):
                target folder
            push (bool):
                files are to be sent (or received)
        Returns:
            (str):
                the rsync command stdout
        """
        from os.path import join
        # can work without a target, but use same folder name as origin
        if target is None:
            target = origin
        # initial setup with command and flags
        cmd = ['rsync', self._rsync_flags.flags]
        # if we're tunnelling, the port has to be specified here
        # also set up target folder connection address if needed
        not_local = True
        if self.is_tunnel:
            cmd.append(f"'-e ssh -p {self.port}'")

            target = f'{self.host}:{target}'
            not_local = False
        elif not self.local:
            target = f'{self.userhost()}:{target}'
            not_local = False

        str = origin if push else target
        end = target if push else origin
        if self.verbose:
            print('sending', files)
            print(f'from {str} to {end}')
        if not_local or push or len(files) == 1:
            for file in files:
                cmd.append(join(str, file))
        else:
            temp = [join(str, '{')]
            for file in files:
                temp.append(file + ',')
            cmd.append(''.join(temp)[:-1] + '}')

        cmd.append(end)
        cmd = ' '.join(cmd)

        return self.cmd(cmd)

    def ensure_dir(self, dir):
        """ensure a directory exists by attempting to create it

        Arguments:
            dir (str):
                directory to create
        """
        if self.local:
            try:
                os.mkdir(dir)
            except FileExistsError:
                pass
        else:
            self.cmd(f'mkdir -p {dir}')

    def test_connection(self, verbose=None):
        """
        Check the connection by testing that we can create directories
        on local, remote and send/receive files

        check file creation permissions for remote and local
        then test that we can create files and rsync them

        Arguments:
            verbose (bool):
                verbosity for this test

        Returns:
            (bool):
                True if test was successful
        """
        from os.path import join
        # override verbosity for these tests
        # (they can produce a lot of output)
        old_verbose = self.verbose
        local_verbose = True
        if verbose is None:
            # minimal verbosity, but not off
            self.verbose = False
        elif verbose:
            # fully on
            self.verbose = True
        elif not verbose:
            # fully off
            self.verbose = False
            local_verbose = False

        if local_verbose:
            print('#'*24)
            print('checking remote permissions')
        remote_fld = self._file_create_validation('/', False, local_verbose)
        if local_verbose:
            print('\n'+'#'*24)
            print('checking local permissions')
        local_fld = self._file_create_validation('/', True, local_verbose)

        if remote_fld == '' or local_fld == '':
            return False

        # test rsync for the created folder
        if local_verbose:
            print('\n' + '#'*24)
            print('checking that we can send files')
        local_test_file = 'test_local.txt'
        local_abspath = join(local_fld, local_test_file)
        self.cmd(f'cat > {local_abspath}', local=True)
        self.rsync([local_test_file], local_fld, remote_fld)
        self.ls(remote_fld)
        success_l = local_test_file in self.ls(f'{remote_fld}', local=False)

        if local_verbose:
            print('\n' + '#'*24)
            print('checking that we can receive files')
        remote_test_file = 'test_remote.txt'
        remote_abspath = join(remote_fld, remote_test_file)
        self.cmd(f'cat > {remote_abspath}')
        self.rsync([remote_test_file], local_fld, remote_fld, push=False)
        self.ls(local_fld, local=True)
        success_r = remote_test_file in self.ls(f'{local_fld}', local=True)

        if local_verbose:
            print('\n' + '#'*24)
            print('cleaning up...')
            print(f'\tremoving folders: '
                  f'\n\tremote: {remote_fld}, '
                  f'\n\tlocal: {local_fld}')
        self.cmd(f'rm -r {remote_fld}')
        self.cmd(f'rm -r {local_fld}', local=True)

        self.verbose = old_verbose

        if success_l and success_r:
            return True
        return False

    def _file_create_validation(self, root, local, verbose):
        from os.path import join
        # permission that we're updating
        permission = 'local_permissions' if local else 'remote_permissions'
        # directory access
        prior = self.ls(root, local=local)
        # generate a folder name that doesn't already exist
        test_folder = 'URL_connection_test'
        i = 0
        timeout = 10
        while test_folder in prior:
            if i > timeout:
                raise ValueError('could not create folder, '
                                 f'try cleaning out {root}')
            i += 1
            if verbose:
                print(f'{test_folder} already exists ({i}/{timeout})')
            test_folder = 'URL_connection_test_' + str(i)
        abspath_folder = join(root, test_folder)
        if verbose:
            print(f'creating test folder {abspath_folder}')

        # # if we're not running in root, need folder break
        # # this / will mess with checks, however, so need a new variable
        # if root[-1] != '/':
        #     fld = '/' + test_folder
        # else:
        #     fld = test_folder
        self.cmd(f'mkdir -p {abspath_folder}', local=local)

        if verbose:
            print(f'looking for {test_folder} in {root}')
        folder_created = test_folder in self.ls(root, local=local)

        # if folder not created in root, can we redirect to user folders?
        if not folder_created:
            if root == '/':
                if verbose:
                    print('\tpermission denied, trying again in user folders')
                if local:
                    homedir = os.environ['HOME']
                else:
                    homedir = self.cmd('echo $HOME').rstrip('\n')
                return self._file_create_validation(f'{homedir}',
                                                    local=local,
                                                    verbose=verbose)
            else:
                if self.verbose:
                    print('\tfailure, returning empty string')
                self._validation[permission] = ''
                return ''
        if self.verbose:
            print(f'success! (local: {local}) returning {abspath_folder}')
        self._validation[permission] = 'root' if root == '/' else 'user'
        return f'{abspath_folder}'


if __name__ == '__main__':

    tunnel = False
    test = URL(rsync='aux')
    if tunnel:
        test.host = '127.0.0.10'
        test.port = 1800
    else:
        test.host = '192.168.0.17'
    test.user = 'pi'

    print(test)
    test.test_connection()
