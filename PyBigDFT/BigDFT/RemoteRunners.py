"""
This module wraps the classes and functions you will need if you want to run
PyBigDFT operations on a remote machine.
"""

try:
    from BigDFT.AiidaCalculator import AiidaCalculator
    AIIDA_FLAG = True
except ModuleNotFoundError:
    AIIDA_FLAG = False

from BigDFT.Calculators import Runner
from BigDFT.Datasets import Dataset
from BigDFT.URL import URL


class CallableAttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(CallableAttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self

    def __call__(self, **kwargs):
        self.update(kwargs)
        return str(self)


def _ensure_url(url, verbose=False):
    """make sure the URL is an actual class

    Arguments:
        url (str, URL):
            url to be enforced to BigDFT.URL

    Returns:
        URL
    """
    if url is None:
        return URL()

    if not isinstance(url, URL):
        # now require a URL handler for remote operations
        # create one if needed
        # warnings.DeprecationWarning is ignored by default, just print
        print('Warning: Explicit url expression is to be deprecated. '
              'Create a url using BigDFT.URL.')
        url = URL.from_string(url)

        url.verbose = verbose

    return url


def _find_in_ls(ls_return, target):
    """Parse the output of a system 'ls', searching for a target file

    ls_return: bytestring return from _system "ls" call in target folder
    target: target file (path or filename)

    Returns:
        bool: presence of target file
    """
    import os
    if '/' in target or r'\\' in target:
        target = os.path.split(target)[-1]

    files = ls_return.split('\n')
    return target in files


def _bind_closure(f, **kwargs):
    """
    Given a function and its named arguments, this routine binds that as
    a lexical context for a closure which is returned.
    """
    return lambda: f(**kwargs)


class RemoteFunction():
    """Serialize and execute remotely a python function.

    With this class we serilize and execute a python function
    on a remote filesystem. This filesystem may be a remote computer
    or a different directory of the local machine.

    Such class is useful to be employed in runners which needs to be executed
    outside of the current framework, butfor which the result is needed to
    continue the processing of the data.

    Args:
        name (str): the name of the remote function
        function (func): the python function to be serialized.
            The return value of the function should be an object for which
            a relatively light-weight serialization is possible.
        submitter (str): the interpreter to be invoked. Should be, e.g.
            `python` if the function is a python function, or `bash`.
        **kwargs:  keyword arguments of the function.
            The arguments of the function should be serializable without
            the requirement of too much disk space.
    """
    def __init__(self, submitter, name, **kwargs):
        self.name = name
        self.submitter = submitter
        self.appended_functions = []
        self.arguments = kwargs
        self.output_files = []
        raise NotImplementedError

    def _set_arguments(self):
        pass

    def _serialize(self, run_dir):
        return []

    def _make_run(self, run_dir):
        self.resultfile = None
        self.runfile = None
        raise NotImplementedError

    def _read_results(self, run_dir):
        return None

    def files_sent(self, yes):
        """
        Mark the relevant files as sent.

        Args:
            yes (bool): True if the files have been already sent to the
                remote directory
        """
        self.files_have_been_sent = yes

    def files_received(self, yes):
        """
        Mark the relevant files as received.

        Args:
            yes (bool): True if the files have been already received in the
                remote directory
        """
        self.files_have_been_received = yes

    def _write_main_runfile(self, pscript, run_dir):
        from os.path import join
        runfile = join(run_dir, self.runfile)
        with open(runfile, "w") as ofile:
            ofile.write(pscript + '\n' + self._append_further_lines())
        return self.runfile

    def _append_further_lines(self):
        pscript = ''
        for func in self.appended_functions:
            pscript += func.call(remotely=False, remote_cwd=True) + '\n'
        return pscript

    def _append_further_files_to_send(self):
        initials = []
        for func in self.appended_functions:
            initials += func.setup(src=self.local_directory)
            self.resultfile = func.resultfile  # override resultfile
        return initials

    def _append_further_files_to_receive(self):
        initials = []
        for func in self.appended_functions:
            initials.append(func.resultfile)  # add the result for info
            initials += func.output_files
        return initials

    def prepare_files_to_send(self):
        """List of files that will have to be sent.

        This function defines the files which requires to be sent over
        in order to execute remotely the python function.
        Also set the main file to be called and the resultfile.

        Returns:
            dict: files which will be sent
        """
        run_dir = self.local_directory
        self._set_arguments()
        initials = self._serialize(run_dir)
        pscript = self._make_run(run_dir)
        dependencies = self._append_further_files_to_send()
        self.main_runfile = self._write_main_runfile(pscript, run_dir)
        initials.append(self.main_runfile)
        self.files_sent(False)
        return initials + dependencies

    def prepare_files_to_receive(self):
        """List of files that will have to be received.

        This function defines the files which requires to be get from the url
        in order to execute remotely the python function.

        Returns:
            dict: files which will be received
        """
        initials = self.output_files
        dependencies = self._append_further_files_to_receive()
        initials.append(self.resultfile)
        self.files_received(False)
        return initials + dependencies

    def setup(self, dest='.', src='/tmp/'):
        """Create and list the files which have to be sent remotely.

        Args:
            dest (str): remote_directory of the destination filesystem.
                The directory should exist and write permission should
                be granted.
            src (str): local directory to prepare the IO files to send.
                The directory should exist and write permission should
                be granted.

        Returns:
            dict: dictionary of {src: dest} files
        """
        self.local_directory = src
        self.remote_directory = dest
        files_to_send = [f for f in self.prepare_files_to_send()]
        self.files_to_send = files_to_send
        files_to_receive = [f for f in self.prepare_files_to_receive()]
        self.resultfiles = files_to_receive
        return files_to_send

    def send_files(self, files):
        """Send files to the remote filesystem to which the class should run.

        With this function, the relevant serialized files should be sent,
        via the rsync protocol (or equivalent) into the remote directory
        which will then be employed to run the function.

        Args:
            files(dict): source and destination files to be sent,
                organized as items of a dictionary.
        """
        if hasattr(self, 'files_have_been_sent') and self.files_have_been_sent:
            pass
        else:
            self.url.rsync(files,
                           self.local_directory,
                           self.remote_directory)
        self.files_sent(True)

    def append_function(self, extra_function):
        """Include the call to another function in the main runfile.

        With this method another remote function can be called in the same
        context of the remote call.
        Such remote function will be called from within the same remote
        directory of the overall function.
        The result of the remote function will then be the result provided
        from the last of the appended functions.

        Args:
            extra_function (RemoteFunction): the remote function to append.
        """
        assert extra_function.name != self.name, \
            'Name of the remote function should be different'
        for func in self.appended_functions:
            assert extra_function.name != func.name, \
                'Cannot append functions with the same name'
        self.appended_functions.append(extra_function)

    def send_to(self, dest='.', src='/tmp/'):
        """Assign the remote filesystem to which the class should run.

        With this function, the relevant serialized files should be sent,
        via the rsync protocol (or equivalent) into the remote directory
        which will then be employed to run the function.

        Args:
            dest (str): remote_directory of the destination filesystem.
                The directory should exist and write permission should
                be granted.
            src (str): local directory to prepare the IO files to send.
                The directory should exist and write permission should
                be granted.
        """
        files = self.setup(dest=dest, src=src)
        self.send_files(files)

    def call(self, remotely=True, remote_cwd=False):
        """Provides the command to execute the function.

        Args:
            remotely (bool): invoke the function from local machine.
                employ ssh protocol to perform the call.
            remote_cwd (bool): True if our CWD is already the remote_dir to
                which the function has been sent. False otherwise.
                Always assumed as False if remotely is True.

        Returns:
            str: the string command to be executed.
        """
        command = self.submitter + ' ' + self.main_runfile
        if remotely or not remote_cwd:
            command = 'cd '+self.remote_directory + ' && ' + command
        return command

    def is_finished(self, remotely=True, anyfile=True,
                    timeout=0, verbose=None):
        """
        Check if the function has been executed.
        This is controlled by the presence of the resultfile of the
        last of the functions dependencies.

        Args:
            remotely (bool): control the presence of the result on the remote
                filesystem
            anyfile (bool): determine if any run is finished. Useful if the run
                in question is asyncronous
            timeout (int): maximum number of times is_finished can be called
                before the check times out, prevents infinite loops.
                Set to -1 to disable check (Dangerous!)
                Set to nonzero integer to limit to `timeout` calls.
        Return:
            bool: True if ready, False otherwise.
        """
        from subprocess import check_output
        from os.path import join, getmtime
        if verbose is None:
            verbose = self.verbose
        if verbose:
            print('Checking for finished run... ', end='')
        remotefile = self.resultfile

        if remotely:
            # better to ask forgiveness...
            try:
                root = self.remote_directory
            except AttributeError as E:
                # no remote file found, probably haven't called run() prior
                # initiate timeout behaviour if enabled
                if timeout >= 0:
                    if not hasattr(self, '_is_finished_timeout'):
                        # first call, set attribute
                        self._is_finished_timeout = 0

                    self._is_finished_timeout += 1

                    if self._is_finished_timeout >= timeout:
                        if verbose:
                            print('Remote run not found (have we run?)')
                        raise AttributeError(E)
                if verbose:
                    print('No (No files found)')
                return False
        else:
            # TODO(lbeal) Propogate timeout feature from remote to local?
            root = self.local_directory

        remotefile = join(root, remotefile)

        # check that a remote results file exists
        # check_output will return a CalledProcessError
        # in the event that none are present
        if not remotely or self.url.local:
            if verbose:
                print('locally... ', end='')
            ret = check_output(["ls", root])
        else:
            if verbose:
                print('remotely... ', end='')
            ret = self.url.cmd('ls ' + root)

        file_present = _find_in_ls(ret, remotefile)

        if not file_present:
            if verbose:
                print('No (No result file)')
            return False  # no file

        # we have a results file, check modification times if we care about
        # the _current_ run
        if not anyfile:
            runfile = join(root, self.runfile)

            # file modification times (unix) of results, and runner
            if not remotely or self.url.local:
                res_mtime = getmtime(remotefile)
                run_mtime = getmtime(runfile)
            else:
                # Get the modified time using stat
                res_mtime = self.url.cmd('stat -c %Y ' + remotefile)
                run_mtime = self.url.cmd('stat -c %Y ' + runfile)

                res_mtime = int(res_mtime)
                run_mtime = int(run_mtime)

            # if the results file was modified after the current run file
            # there is likely a run still in progress, so return false
            if res_mtime < run_mtime:
                if verbose:
                    print('No (Outdated resultsfile)')
                return False  # there's a file, but from before this run

            if verbose:
                print('Yes (And recent run)')
            self.status = 'Finished'
            return True  # res file, and from after the last run

        if verbose:
            print('Yes (Found a results file)')
        self.status = 'Finished'
        return True  # file, and we care about any run

    def receive_files(self, files):
        if hasattr(self, 'files_have_been_received') and \
                self.files_have_been_received:
            pass
        else:
            self.url.rsync(files,
                           self.local_directory,
                           self.remote_directory,
                           push=False)
        self.files_received(True)

    def fetch_result(self, remotely=True):
        """Get the results of a calculation locally.

        Args:
            remotely (bool): control the presence of the result on the remote
                filesystem

        Returns:
            The object returned by the original function
        """

        if self.protocol == 'aiida':
            # is_finished does not work for an aiida run
            print('retrieving stored aiida run')
            return self._read_results('')

        elif not self.is_finished(remotely=remotely):
            raise ValueError("Calculation not completed yet")

        files = self.resultfiles
        self.receive_files(files)

        if len(self.appended_functions) > 0:
            func = self.appended_functions[-1]
        else:
            func = self
        # Read results, with dependencies or not
        return func._read_results(self.local_directory)


class RemoteDillFunction(RemoteFunction):
    """Serialize and execute remotely a python function, serialized with dill.

    With this class we serilize and execute a python function
    on a remote filesystem. This filesystem may be a remote computer
    or a different directory of the local machine.

    Such class is useful to be employed in runners which needs to be executed
    outside of the current framework, butfor which the result is needed to
    continue the processing of the data.

    Args:
        name (str): the name of the remote function
        function (func): the python function to be serialized.
            The return value of the function should be an object for which
            a relatively light-weight serialization is possible.
        submitter (str): the interpreter to be invoked. Should be, e.g.
            `python` if the function is a python function, or `bash`.
        required_files (list): list of extra files that may be required for
            the good running of the function.
        output_files (list): list of the files that the function will produce
            that are supposed to be retrieved to the host computer.
        **kwargs:  keyword arguments of the function.
            The arguments of the function should be serializable without
            the requirement of too much disk space.
    """
    def __init__(self, submitter, name, function, required_files=[],
                 output_files=[], **kwargs):
        self.name = name
        self.submitter = submitter
        self.appended_functions = []
        self.required_files = []
        self.arguments = kwargs
        self.raw_function = function
        self.output_files = output_files

    def _set_arguments(self):
        self.function = _bind_closure(self.raw_function, **self.arguments)

    def _serialize(self, run_dir):
        from os.path import join
        from dill import dump
        self.serialized_file = self.name + "-serialize.dill"
        serializedfile = join(run_dir, self.serialized_file)
        with open(serializedfile, "wb") as ofile:
            dump(self.function, ofile, recurse=False)
        return [self.serialized_file] + self.required_files

    def _make_run(self, run_dir):
        self.resultfile = self.name + '-result.dill'
        self.runfile = self.name + "-run.py"
        pscript = ["# Run the serialized script\n"]
        pscript.append("from dill import load, dump\n")
        pscript.append("with open(\""+self.serialized_file+"\" , \"rb\")")
        pscript.append(" as ifile:\n")
        pscript.append("    fun = load(ifile)\n")
        pscript.append("result = fun()\n")
        pscript.append("with open(\""+self.resultfile+"\" , "
                                                      "\"wb\") as ofile:\n")
        pscript.append("    dump(result, ofile)\n")
        return ''.join(pscript)

    def _read_results(self, run_dir):
        from dill import load
        from os.path import join
        with open(join(run_dir, self.resultfile), "rb") as ifile:
            res = load(ifile)
        return res

    def append_function(self, extra_function):
        raise NotImplementedError('Cannot append a function.' +
                                  ' Use a RemoteScript to concatenate')


class RemoteJSONFunction(RemoteFunction):
    """Serialize and execute remotely a python function, serialized with JSON.

    With this class we serilize and execute a python function
    on a remote filesystem. This filesystem may be a remote computer
    or a different directory of the local machine.

    Such class is useful to be employed in runners which needs to be executed
    outside of the current framework, for which the result is needed locally to
    continue data processing.

    Args:
        name (str): the name of the remote function
        function (func): the python function to be serialized.
            The return value of the function should be an object for which
            a relatively light-weight serialization is possible.
        submitter (str): the interpreter to be invoked. Should be, e.g.
            `python` if the function is a python function, or `bash`.
        extra_encoder_functions (list)): list of dictionaries of the format
            {'cls': Class, 'func': function} which is employed to serialize
            non-instrinsic objects as well as non-numpy objects.
        required_files (list): list of extra files that may be required for
            the good running of the function.
        output_files (list): list of the files that the function will produce
            that are supposed to be retrieved to the host computer.
        **kwargs:  keyword arguments of the function.
            The arguments of the function should be serializable without
            the requirement of too much disk space.
    """
    def __init__(self, submitter, name, function, extra_encoder_functions=[],
                 required_files=[], output_files=[], **kwargs):
        from inspect import getsource
        self.name = name
        self.function = getsource(function)
        self.function_name = function.__name__
        self.extra_encoder_functions = extra_encoder_functions
        self.encoder_functions_serialization = {s['cls'].__name__:
                                                {'name': s['func'].__name__,
                                                 'source':
                                                 getsource(s['func'])}
                                                for s in
                                                extra_encoder_functions}
        self.required_files = required_files
        self.submitter = submitter
        self.appended_functions = []
        self.arguments = kwargs
        self.output_files = output_files

    def _set_arguments(self):
        from futile.Utils import serialize_objects
        self.arguments_serialization = serialize_objects(
            self.arguments, self.extra_encoder_functions)

    def _serialize(self, run_dir):
        from os.path import join
        from futile.Utils import create_tarball
        if len(self.required_files) + len(self.arguments_serialization) == 0:
            self.serialized_file = ''
            return []
        self.serialized_file = self.name + "-files.tar.gz"
        serializedfile = join(run_dir, self.serialized_file)
        create_tarball(serializedfile, [],
                       {k+'.json': v
                        for k, v in self.arguments_serialization.items()})
        return [self.serialized_file] + self.required_files

    def _make_run(self, run_dir):
        self.resultfile = self.name + '-result.json'
        self.runfile = self.name + "-run.py"
        pscript = ["kwargs = {}\n"]
        pscript.append("import json\n")
        if self.serialized_file != '':
            pscript.append("# Unpack the argument tarfile if present\n")
            pscript.append("import tarfile\n")
            pscript.append("# extract the archive\n")
            pscript.append("arch = tarfile.open('" + self.serialized_file +
                           "')\n")
            pscript.append("#arch.extractall(path='.')\n")
            # pscript.append("files = arch.getnames()\n")
            for arg in self.arguments_serialization.keys():
                pscript.append("#with open('" + arg + ".json', 'r') as f:\n")
                pscript.append("with arch.extractfile('" + arg + ".json') as "
                                                                 "f:\n")
                pscript.append("    kwargs['" + arg + "'] = json.load(f)\n")
            pscript.append("arch.close()\n")
        pscript.append("extra_encoder_functions = []\n")
        for name, func in self.encoder_functions_serialization.items():
            pscript.append(func['source'])
            pscript.append("extra_encoder_functions.append({'cls'" + name +
                           ", 'func:'" + func['name'] + "})\n")
        pscript.append("class CustomEncoder(json.JSONEncoder):\n")
        pscript.append("    def default(self, obj):\n")
        pscript.append("        try:\n")
        pscript.append("            import numpy as np\n")
        pscript.append("            nonumpy = False\n")
        pscript.append("        except ImportError:\n")
        pscript.append("            nonumpy = True\n")
        pscript.append("        if not nonumpy:\n")
        pscript.append("            if isinstance(obj, (np.int_, np.intc,\n")
        pscript.append("                                np.intp, np.int8,\n")
        pscript.append("                                np.int16, np.int32,\n")
        pscript.append("                                np.int64, np.uint8,\n")
        pscript.append("                                np.uint16, "
                       "np.uint32,\n")
        pscript.append("                                np.uint64)):\n")
        pscript.append("                return int(obj)\n")
        pscript.append("            elif isinstance(obj, (np.float_, "
                       "np.float16,\n")
        pscript.append("                                  np.float32,\n")
        pscript.append("                                  np.float64)):\n")
        pscript.append("                return float(obj)\n")
        pscript.append("            elif isinstance(obj, (np.ndarray,)):\n")
        pscript.append("                return obj.tolist()\n")
        pscript.append("        if isinstance(obj, (set,)):\n")
        pscript.append("            return list(obj)\n")
        pscript.append("        else:\n")
        pscript.append("            for spec in extra_encoder_functions:\n")
        pscript.append("                if isinstance(obj, (spec['cls'],)):\n")
        pscript.append("                    return spec['func'](obj)\n")
        pscript.append("        return json.JSONEncoder.default(self, obj)\n")
        pscript.append(self.function)
        pscript.append("result = "+self.function_name+"(**kwargs)\n")
        pscript.append("with open(\""+self.resultfile+"\" , \"w\") as "
                                                      "ofile:\n")
        pscript.append("    json.dump(result, ofile, cls=CustomEncoder)\n")
        return ''.join(pscript)

    def _read_results(self, run_dir):
        from json import load
        from os.path import join
        with open(join(run_dir, self.resultfile), "r") as ifile:
            res = load(ifile)
        return res

    def append_function(self, extra_function):
        raise NotImplementedError('Cannot append a function.' +
                                  ' Use a RemoteScript to concatenate')


if AIIDA_FLAG:
    class RemoteAiidaFunction(AiidaCalculator):
        """Pass a function to a remote machine using the AiiDA infrastructure

        """

        def __init__(self, name, **kwargs):
            self.name = name
            self.remotely = False  # yes, but also no
            self.resultfile = AIIDA_FLAG
            self.output_files = []

            # convert kwargs into args used by AiidaCalculator
            # TODO(lbeal): find a more elegant way of doing this
            AiiDA_arg_translate = {'mpi_procs_per_machine': 'NMPIVAR',
                                   'omp': 'NOMPVAR',
                                   }
            self.AiiDA_args = {'code': 'bigdft@localhost'}
            for a, k in AiiDA_arg_translate.items():
                if k in kwargs:
                    self.AiiDA_args[a] = kwargs.get(k)

        def setup(self, **kwargs):
            """Create a remote Aiida Function by passing values to constructor
            """
            super().__init__(**self.AiiDA_args)
            # a dict containing files to send is expected, return empty dict
            return {}

        def call(self, **kwargs):
            """Usually provides the command to execute the function.

            In this case, calling is handled by AiiDA,
            so is just a placeholder.

            Args:
                kwargs: Dummy holder to accept keyword args.

            Returns:
                str: empty str, calling is handled by aiida
            """

            return ''

        def process_run(self, **kwargs):
            # don't run if we don't have to
            # TODO(lbeal): add an alternative "if run_options['skip']:"
            if hasattr(self, '_result'):
                return {'status': 'finished_remotely'}

            # because of name mangling this process_run takes priority over
            # the one in AiidaCalculator. Call that one instead if needed
            inp = kwargs.get('input', None)
            if inp is None:
                return super().process_run(**kwargs)
            # call the run method of the aiida calculator
            self._result = self.run(**kwargs)

            return {'status': 'submitted'}

        def _read_results(self):

            return self._result


class RemoteScript(RemoteFunction):
    """Triggers the remote execution of a script.

    This class is useful to execute remotely a script and to retrieve.
    The results of such execution. It inherits from the `RemoteFunction`
    base class and extends some of its actions to the concept of the script.

    Args:
        name (str): the name of the remote function
        script (str, func): The script to be executed provided in string form.
            It can also be provided as a function which returns a string.
        result_file(str): the name of the file in which the script
            should redirect.
        submitter (str): the interpreter to be invoked. Should be, e.g.
            `bash` if the script is a shell script, or 'qsub' if this is a
            submission script.
        output_files (list): list of the files that the function will produce
            that are supposed to be retrieved to the host computer.
        **kwargs:  keyword arguments of the script/script function,
            which will be substituted in the string representation.
    """
    def __init__(self, submitter, name, script, result_file, output_files,
                 **kwargs):
        self.name = name
        self.script = script
        self.submitter = submitter
        self.resultfile = result_file
        self.arguments = kwargs
        self.appended_functions = []
        self.output_files = output_files

    def _set_arguments(self):
        if isinstance(self.script, str):
            scr = self.script
            for key, value in self.arguments.items():
                scr = scr.replace(key, str(value))
            self.script = scr
        else:
            self.script = self.script(**self.arguments)

    def _read_results(self, run_dir):
        if self.protocol == 'aiida':  # dirty deferrence
            return self.remote_function._read_results()
        from os.path import join
        with open(join(run_dir, self.resultfile), 'rb') as ifile:
            res = [line.decode('utf-8') for line in ifile.readlines()]
        return res

    def _make_run(self, run_dir):
        self.runfile = self.name + "-run.sh"
        return self.script


class RemoteRunner(Runner, RemoteScript):
    """
    This class can be used to run python functions on a remote machine. This
    class combines the execution of a script with a python remote function.

    Args:

        function (func): the python function to be serialized.
            The return value of the function should be an object for which
            a relatively light-weight serialization is possible.
        name (str): the name of the remote function. Function name is employed
            if not provided.
        script (str): The script to be executed provided in string form.
            The result file of the script is assumed to be named
            `<name>-script-result`.
        submitter (str): the interpreter to be invoked. Should be, e.g.
            `bash` if the script is a shell script, or `qsub` if this is a
            submission script.
        url (str, URL): either a user@host string
            or URL() connection class (preferable)
        remote_dir (str): the path to the work directory on the remote machine.
           Should be associated to a writable directory.
        skip (bool): if true, we perform a lazy calculation.
        asynchronous (bool): If True, submit the calculation without waiting
           for the results.
        local_dir (str): local directory to prepare the IO files to send.
            The directory should exist and write permission should
            be granted.
        python (str): python interpreter to be invoked in the script.
        protocol (str): serialization method to be invoked for the function.
            can be 'JSON' of 'Dill', depending of the desired version.
        extra_encoder_functions (list)): list of dictionaries of the format
            {'cls': Class, 'func': function} which is employed to serialize
            non-instrinsic objects as well as non-numpy objects. Useful for
            the 'JSON' protocol.
        required_files (list): list of extra files that may be required for
            the good running of the function.
        output_files (list): list of the files that the function will produce
            that are supposed to be retrieved to the host computer.
        arguments (dict):  keyword arguments of the function.
            The arguments of the function should be serializable without
            the requirement of too much disk space. Such arguments cannot
            be named in the same way as the others.
        **kwargs (dict):  Further keyword arguments of the script,
            which will be substituted in the string representation.
    """

    def __init__(self, function=None, submitter='bash', name=None,
                 url=None, skip=True, asynchronous=True, remote_dir='.',
                 rsync='', local_dir='/tmp',
                 script="#!/bin/bash\n", python='python', arguments={},
                 protocol='JSON', extra_encoder_functions=[],
                 required_files=[], output_files=[], **kwargs):
        self.protocol = protocol.lower()  # remove case sensitivity

        # get verbose from kwargs, default True
        self.verbose = kwargs.get('verbose', True)
        url = _ensure_url(url, self.verbose)

        if rsync == '':  # update rsync based on verbosity if not specified
            if self.verbose:
                rsync = '-auv'
            else:
                rsync = '-auq'
        url.rsync_flags = rsync
        self.url = url

        if function is None:  # function is always AiidaCalculator for aiida
            if self.protocol != 'aiida':
                raise ValueError('Function can only be '
                                 'None for AiiDA based runs')
            if name is None:  # set name if none specified
                name = 'AiiDA-calc'
        if name is None:
            name = function.__name__
        super().__init__(submitter=submitter, name=name, script=script,
                         result_file=name + '-script-result',
                         url=url, skip=skip, asynchronous=asynchronous,
                         remote_dir=remote_dir, local_dir=local_dir,
                         protocol=self.protocol, python=python,
                         extra_encoder_functions=extra_encoder_functions,
                         required_files=required_files, function=function,
                         arguments=arguments, output_files=output_files,
                         **kwargs)
        self.remote_function = self._create_remote_function(
            name, self._global_options)
        self.append_function(self.remote_function)

    def _create_remote_function(self, name, options):
        rfargs = {'submitter': options['python'],
                  'name': name + '-function',
                  'function': options['function'],
                  'required_files': options['required_files']}
        protocol = options['protocol']

        dispatch = {'json': RemoteJSONFunction,
                    'dill': RemoteDillFunction}
        if AIIDA_FLAG:  # if aiida is in the env, add it to the dispatch table
            dispatch['aiida'] = RemoteAiidaFunction
        if protocol not in dispatch:
            raise ValueError('protocol must be one of',
                             list(dispatch.keys()))

        cls = dispatch[protocol]
        if protocol == 'json':
            rfargs.update(options['extra_encoder_functions'])
        if protocol == 'aiida':
            update = ['NMPIVAR', 'NOMPVAR']
            for key in [k for k in update if k in options]:
                print(f'passed key {key} to remote')
                rfargs[key] = options[key]

        # all the keys of the instantiated class so far are protected.
        self.protected_arguments = list(rfargs.keys())
        reargs = self._check_protected_arguments(options['arguments'])
        rfargs.update(reargs)
        return cls(**rfargs)

    def _check_protected_arguments(self, args):
        for arg in args:
            if arg in self.protected_arguments:
                raise ValueError("Keyword argument '" + arg +
                                 "' cannot be named this way (name clashing)")
        return args

    def pre_processing(self):
        reargs = self._check_protected_arguments(self.run_options['arguments'])
        self.remote_function.arguments = reargs
        if hasattr(self, 'files_to_send'):
            files_to_send = self.files_to_send
        else:
            files_to_send = self.setup(dest=self.run_options['remote_dir'],
                                       src=self.run_options['local_dir'])
        return {'files': files_to_send}

    def _get_opts(self, opts):
        return {key: self.run_options[key] for key in opts}

    def process_run(self, files):
        # allow passing of anyfile option, default to False
        anyfile = self.run_options.get('anyfile', True)
        # if force, disable skip
        force = self.run_options.get('force', False)
        if self.protocol == 'aiida':
            return self.remote_function.process_run(**self.local_options)
        if self.run_options['skip'] and not force and self.url.local:
            if self.is_finished(remotely=False,
                                anyfile=anyfile,
                                verbose=False):
                self.status = 'finished_locally'
                # touch the files to prevent infinite is_finished() loops
                rfile = self.local_directory + '/' + self.resultfile
                self.url.cmd(f'touch {rfile}')
                return {'status': 'finished_locally'}
        self.send_files(files)
        if self.run_options['skip'] and not force:
            if self.is_finished(anyfile=anyfile, verbose=False):
                self.status = 'finished_remotely'
                rfile = self.remote_directory + '/' + self.resultfile
                self.url.cmd(f'touch {rfile}')
                return {'status': 'finished_remotely'}
        command = self.call()
        asynchronous = False
        if self.run_options['asynchronous'] and self.url.local:
            asynchronous = True  # command += ' &'
        self.url.cmd(command, asynchronous=asynchronous)
        self.status = 'submitted'
        return {'status': 'submitted'}

    def post_processing(self, files, status):
        if self.protocol == 'aiida':
            return self.remote_function._result
        if self.run_options['asynchronous']:
            if status == 'finished_locally':
                if self.verbose:
                    print('Data are retrieved from local directory')
                return self.fetch_result(remotely=False)
            elif status == 'finished_remotely':
                return self.fetch_result(remotely=True)
        else:
            return self.fetch_result()


class RemoteDataset(Dataset):
    """
    Defines a set of remote runs, to be executed from a base script and to
    a provided url. This class is associated to a set of remote submissions,
    which may contain multiple calculations. All those calculations are
    expressed to a single url, with a single base script, but with a collection
    of multiple remote runners that may provide different arguments.

    Args:
        label (str): man label of the dataset.
        run_dir (str): local directory of preparation of the data.
        database_file (str): name of the database file to keep track of the
            submitted runs.
        force (str): force the execution of the dataset regardless of the
            database status.
        **kwargs: global arguments of the appended remote runners.
    """
    def __init__(self, label='RemoteDataset', run_dir='/tmp',
                 database_file='database.yaml', force=False,
                 **kwargs):
        Dataset.__init__(self, label=label, run_dir=run_dir, force=force,
                         **kwargs)
        self.database = RunsDatabase(database_file)
        self.protocol = kwargs.get('protocol', None)
        self.verbose = kwargs.get('verbose', True)
        self.url = _ensure_url(kwargs.get('url', None), self.verbose)
        # self.database_file = database_file
        # self.database = self._construct_database(self.database_file)

    def append_run(self, id, remote_runner=None, **kwargs):
        """Add a remote run into the dataset.

        Append to the list of runs to be performed the corresponding runner and
           the arguments which are associated to it.

        Args:
          id (dict): the id of the run, useful to identify the run in the
             dataset. It has to be a dictionary as it may contain
             different keyword. For example a run might be classified as
             ``id = {'hgrid':0.35, 'crmult': 5}``.
          remote_runner (RemoteRunner): a instance of a remote runner that will
              be employed.
          **kwargs: arguments required for the creation of the corresponding
              remote runner. If remote_runner is provided, these arguments will
              be They will be combined with the global arguments.

        Raises:
          ValueError: if the provided id is identical to another previously
             appended run.
        """
        from os.path import join
        # first fill the internal database
        # 'inp' causes problems with aiida, must translate to 'input'
        if 'inp' in kwargs['arguments'] and self.protocol == 'aiida':
            vals = kwargs['arguments'].pop('inp')
            kwargs['arguments']['input'] = vals
        Dataset.append_run(self, id, Runner(), **kwargs)
        # then create the actual remote runner to be included
        inp = self.runs[-1]
        local_dir = inp.get('local_dir')
        if local_dir is not None:
            basedir = join(self.get_global_option('run_dir'), local_dir)
        else:
            basedir = self.get_global_option('run_dir')
        inp['local_dir'] = basedir
        # for some reason a prior RemoteRunner.run() can add it's own output
        # file here. Force it to be empty.
        inp['output_files'] = []
        name = self.names[-1]
        # the arguments have to be substituted before the run call
        if remote_runner is not None:
            remote_script = remote_runner
            remote_script.name = name
        else:
            remote_script = RemoteRunner(name=name, **inp)
        self.calculators[-1]['calc'] = remote_script

    def pre_processing(self):
        from warnings import warn

        def get_info_from_runner(irun, info):
            run_inp = self.runs[irun]
            val = run_inp.get(info, remote_runner._global_options.get(info))
            return val

        def get_local_run_info(irun, info):
            return self.run_options.get(info, get_info_from_runner(irun, info))

        # gather all the data to be sent
        files_to_send = {}
        selection = []
        force = self.run_options['force']
        for irun, (name, calc) in enumerate(zip(self.names, self.calculators)):
            # Check the database.
            do_it = True
            if not force:
                if self.database.exists(name):  # self._check_database(name):
                    run_dir = self.get_global_option('run_dir')
                    warn(str((name, run_dir)) + " already submitted",
                         UserWarning)
                    do_it = False
            if do_it:
                selection.append(irun)
            else:
                continue
            remote_runner = calc['calc']
            local_dir = remote_runner.get_global_option('local_dir')
            self.url.ensure_dir(local_dir)
            remote_dir = get_info_from_runner(irun, 'remote_dir')
            if remote_dir not in files_to_send:
                files_to_send[remote_dir] = []
            files_to_send[remote_dir].extend(remote_runner.setup(
                dest=remote_dir, src=local_dir))
        # then send the data and mark them as sent.
        for remote, files in files_to_send.items():
            self.url.rsync(files, local_dir, remote)
        for irun, calc in enumerate(self.calculators):
            self.calculators[irun]['calc'].files_sent(True)
        self.selection = selection
        return {}

    def process_run(self):
        """Run the dataset, by performing explicit run of each of the item of
        the runs_list.
        """
        self._run_the_calculations(selection=self.selection)
        # Also, update the database informing that
        # the data should run by now.
        for irun, name in enumerate(self.names):
            if irun not in self.selection:
                continue
            self.database.register(name)  # self._register_database(name)
        return {}

    def is_finished(self, anyfile=True, verbose=False):
        """Returns all() of is_finished methods of each runner present

        Args:
            anyfile (bool): Checks for file recency if False

            verbose (bool): Will not print checking status if False

        Returns:
            bool: True if all runs have completed
        """
        fin = True
        for irun, calc in enumerate(self.calculators):
            if irun in self.selection:
                fin = fin and \
                      calc['calc'].is_finished(
                          anyfile=anyfile, verbose=verbose)

        # fin = [run['calc'].is_finished(anyfile=anyfile, verbose=verbose) for
        #        run in self.calculators]

        return fin

    # def _check_database(self, name):
    #     return name in self.database
    #
    # def _register_database(self, name):
    #     if name not in self.database:
    #         self.database.append(name)
    #         self._write_database()
    #
    # def _construct_database(self, name):
    #     from yaml import load, Loader
    #     try:
    #         with open(name, "r") as ifile:
    #             return load(ifile, Loader=Loader)
    #     except FileNotFoundError:
    #         return []
    #
    # def clean_database(self):
    #     """Remove the database information."""
    #     for name in list(self.database):
    #         self._remove_database_entry(name)
    #
    # def _remove_database_entry(self, name):
    #     if name in self.database:
    #         self.database.remove(name)
    #     self._write_database()
    #
    # def _write_database(self):
    #     from yaml import dump
    #     # Update the file
    #     with open(self.database_file, "w") as ofile:
    #         dump(self.database, ofile)


class RunsDatabase():
    """Contains the list of runs which have been submitted.

    Args:
        database_file (str): name of the file used to store the data
    """
    def __init__(self, database_file):
        self.filename = self._ensure_name(database_file)
        self.database = self._construct_database(self.filename)

    def _ensure_name(self, name):
        """forces the database file to be a .yaml file"""
        # not a yaml file
        if not name.endswith('.yaml'):
            if '.' in name:
                yamlfile = '.'.join(name.split('.')[:-1]) + '.yaml'
            else:
                yamlfile = name + '.yaml'
        else:
            yamlfile = name

        return yamlfile

    def _construct_database(self, name):
        from yaml import load, Loader
        try:
            with open(name, "r") as ifile:
                return load(ifile, Loader=Loader)
        except FileNotFoundError:
            return []

    def _write_database(self):
        from yaml import dump
        # Update the file
        with open(self.filename, "w") as ofile:
            dump(self.database, ofile)

    def exists(self, name):
        """Controls if a name exists in the database."""
        return name in self.database

    def register(self, name):
        """Include a name in the database."""
        if name not in self.database:
            self.database.append(name)
            self._write_database()

    def clean(self):
        """Remove the database information."""
        for name in list(self.database):
            self._remove_database_entry(name)

    def _remove_database_entry(self, name):
        if name in self.database:
            self.database.remove(name)
        self._write_database()


def computer_runner(func, submission_script, url, **kwargs):
    """Create a runner based on a computer information.

    A function is transformed into a remote runner from the specification
    of a computer.

    Args:
        func (func): function to be transformed
        submission_script (CallableAttrDict): specification dictionary of the
            computer. Should contain the attribute ``submitter``.
        url (BigDFT.URL.URL): The url of the computer
        **kwargs: arguments of the RemoteRunner
    """
    from BigDFT.RemoteRunners import RemoteRunner as R
    return R(func, submitter=submission_script.submitter, url=url,
             script=submission_script(), **kwargs)


#     def block_all(self, names, run_dir="."):
#         """
#         Block until all of the calculations in a list are completed.
#
#         Args:
#             names (list): a list of calculation names.
#             run_dir (str): the directory the calculations were run in.
#
#         Returns:
#             A dictionary mapping names to results.
#         """
#         from time import sleep
#         results = {}
#         while any([x not in results for x in names]):
#             for n in names:
#                 if n in results:
#                     continue
#                 if self.is_finished(n, run_dir):
#                     results[n] = self.get_results(n, run_dir)
#             sleep(5.0)
#
#         return results
#
#     def get_results(self, name, run_dir="."):
#         """
#         Get the results of a calculation locally.
#
#         name (str): the name of the calculation that was run.
#         run_dir (str): the work directory.
#
#         Returns:
#           Whatever object that was created remotely.
#         """
#         from os.path import join
#
#         if not self.is_finished(name, run_dir):
#             raise ValueError("Calculation not completed yet")
#
#         # Sync Back
#         self._system(self.rsync_command +
#                      join(self.remote_dir, run_dir, name + "-result.dill") +
#                      " " +
#                      run_dir + "/")
#
#         # Clear the database
#         # self._remove_database_entry(name)
#
#         # Read results
#         return self._read_results(name, run_dir)
#
#     def _ensure_dir(self, run_dir):
#         from os import mkdir
#         from os.path import join
#
#         # Ensure locally
#         try:
#             mkdir(run_dir)
#         except FileExistsError:
#             pass
#
#         # Ensure remotely
#         self._system(self.connection_command + " mkdir -p " +
#                      join(self.remote_dir, run_dir))
